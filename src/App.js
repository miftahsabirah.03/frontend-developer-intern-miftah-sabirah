import React from 'react';
import './index.css';
import Header from "./components/header";
import Banner from './components/banner';
import ListPost from './components/listPost'; 

function App() {
  return (
    <div className="bg-primary text-secondary min-h-screen font-sans">
      <Header />
      <Banner />
      <ListPost /> 
    </div>
  );
}

export default App;
