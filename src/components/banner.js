import React, { useState, useEffect } from 'react';

const Banner = () => {
    const [scrollY, setScrollY] = useState(0);

    useEffect(() => {
        let ticking = false;

        const handleScroll = () => {
            if (!ticking) {
                window.requestAnimationFrame(() => {
                    setScrollY(window.scrollY);
                    ticking = false;
                });
                ticking = true;
            }
        };

        window.addEventListener('scroll', handleScroll);

        return () => {
            window.removeEventListener('scroll', handleScroll);
        };
    }, []);

    return (
        <div className="relative w-full h-screen flex items-center justify-center overflow-hidden">
            <div className="absolute inset-0 w-full h-full overflow-hidden">
                <img 
                    src="./banner-suitmedia.png" 
                    alt="Background" 
                    className="w-full h-full filter brightness-50"
                    style={{ transform: `translateY(-${scrollY * 0.1}px)` }}
                />
            </div>

            <div className="absolute inset-x-0 bottom-[-70px] h-48 bg-white transform skew-y-[-6deg]"></div>
            
            <div className="text-center text-white z-10 relative" style={{ transform: `translateY(${scrollY * 0.5}px)` }}>
                <h1 className="text-5xl font-bold">Ideas</h1>
                <p className="text-xl mt-2">Where all our great things begin</p>
            </div>
        </div>
    );
};

export default Banner;
