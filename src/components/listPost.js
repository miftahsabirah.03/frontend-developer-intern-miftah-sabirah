import React, { useState, useEffect } from 'react';
import axios from 'axios';

const Card = ({ imageSrc, date, title, description }) => (
    <div className="bg-white rounded-lg shadow-md overflow-hidden">
        <img 
            src={imageSrc} 
            alt="Card Image" 
            className="w-full h-48 object-cover" 
            loading="lazy" 
        />
        <div className="p-4">
            <p className="text-gray-500 text-sm">{new Date(date).toLocaleDateString()}</p>
            <h3 className="text-gray-900 font-bold text-lg line-clamp-3 overflow-hidden overflow-ellipsis">{title}</h3>
            <p className="text-gray-700 text-sm">{description}</p>
        </div>
    </div>
);

const Pagination = ({ currentPage, totalPages, onPageChange }) => {
    const generatePageNumbers = () => {
        const pages = [];
        const delta = 2;

        const start = Math.max(1, currentPage - delta);
        const end = Math.min(totalPages, start + 2 * delta);

        for (let i = start; i <= end; i++) {
            pages.push(i);
        }

        return pages;
    };

    const pages = generatePageNumbers();

    return (
        <div className="flex justify-center items-center mt-6">
            <button
                className="px-3 py-1 "
                onClick={() => onPageChange(1)}
                disabled={currentPage === 1}
            >
                &laquo;
            </button>
            <button
                className="px-3 py-1 "
                onClick={() => onPageChange(currentPage - 1)}
                disabled={currentPage === 1}
            >
                &lsaquo;
            </button>
            {pages.map((page) => (
                <button
                    key={page}
                    className={`px-3 py-1 ${page === currentPage ? 'bg-orange-500 text-white rounded-md' : 'bg-white text-gray-700 hover:bg-gray-200 hover:rounded-md'}`}
                    onClick={() => onPageChange(page)}
                >
                    {page}
                </button>
            ))}
            <button
                className="px-3 py-1 "
                onClick={() => onPageChange(currentPage + 1)}
                disabled={currentPage === totalPages}
            >
                &rsaquo;
            </button>
            <button
                className="px-3 py-1 "
                onClick={() => onPageChange(totalPages)}
                disabled={currentPage === totalPages}
            >
                &raquo;
            </button>
        </div>
    );
};



const ListPost = () => {
    const [cards, setCards] = useState([]);
    const [sortOrder, setSortOrder] = useState('-published_at');
    const [itemsPerPage, setItemsPerPage] = useState(10);
    const [currentPage, setCurrentPage] = useState(1);
    const [totalPages, setTotalPages] = useState(1);
    const [totalItems, setTotalItems] = useState(0);

    useEffect(() => {
        const fetchCards = async () => {
            try {
                const response = await axios.get(`https://suitmedia-backend.suitdev.com/api/ideas`, {
                    params: {
                        'page[number]': currentPage,
                        'page[size]': itemsPerPage,
                        'append[]': ['small_image', 'medium_image'],
                        sort: sortOrder
                    }
                });
                const data = response.data;
                setCards(data.data);
                setTotalItems(data.meta.total);
                setTotalPages(Math.ceil(data.meta.total / itemsPerPage));
            } catch (error) {
                console.error('Error fetching data:', error);
            }
        };

        fetchCards();
    }, [sortOrder, itemsPerPage, currentPage]);

    const handleSortChange = (event) => {
        const newSortOrder = event.target.value === 'newest' ? '-published_at' : 'published_at';
        setSortOrder(newSortOrder);
    };

    const handleItemsPerPageChange = (event) => {
        setItemsPerPage(Number(event.target.value));
        setCurrentPage(1); // Reset to the first page whenever items per page change
    };

    const handlePageChange = (page) => {
        setCurrentPage(page);
    };

    const startIndex = (currentPage - 1) * itemsPerPage;

    return (
        <div className="container mx-auto p-4 px-8 md:px-24">
            <div className="flex flex-col md:flex-row md:items-center md:justify-between mb-6">
                <p className='mb-8 md:mb-0'>Showing {startIndex + 1} - {Math.min(startIndex + itemsPerPage, totalItems)} of {totalItems}</p>
                <div className="flex flex-col md:flex-row md:items-center space-y-2 md:space-y-0 md:space-x-4">
                    <div className="flex items-center md:w-auto">
                        <label className="mr-2">Show per page:</label>
                        <div className="relative">
                            <select
                                className="border border-gray-300 rounded-full p-1 px-4 appearance-none pr-14"
                                value={itemsPerPage}
                                onChange={handleItemsPerPageChange}
                            >
                                <option value={10}>10</option>
                                <option value={20}>20</option>
                                <option value={50}>50</option>
                            </select>
                            <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                                <svg className="w-4 h-4 fill-current" viewBox="0 0 20 20">
                                    <path d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" />
                                </svg>
                            </div>
                        </div>
                    </div>
                    <div className="flex items-center md:w-auto">
                        <label className="mr-2">Sort by:</label>
                        <div className="relative">
                            <select
                                className="border border-gray-300 rounded-full p-1 px-4 appearance-none pr-14"
                                value={sortOrder === '-published_at' ? 'newest' : 'oldest'}
                                onChange={handleSortChange}
                            >
                                <option value="newest">Newest</option>
                                <option value="oldest">Oldest</option>
                            </select>
                            <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                                <svg className="w-4 h-4 fill-current" viewBox="0 0 20 20">
                                    <path d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" />
                                </svg>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 gap-4">
                {cards.map((card, index) => (
                    <Card
                        key={index}
                        imageSrc={card.small_image}
                        date={card.published_at}
                        title={card.title}
                        description={card.description}
                    />
                ))}
            </div>
            <Pagination
                currentPage={currentPage}
                totalPages={totalPages}
                onPageChange={handlePageChange}
            />
        </div>

    );
};

export default ListPost;
