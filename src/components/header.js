import React, { useState, useEffect, useRef } from 'react';

const Header = () => {
    const [isScrolled, setIsScrolled] = useState(false);
    const [isMenuOpen, setIsMenuOpen] = useState(false);
    const [activeMenu, setActiveMenu] = useState('ideas'); // ini buat menu aktif pada tampilan awal
    const menuRef = useRef(null);

    useEffect(() => {
        const handleScroll = () => {
            setIsScrolled(window.scrollY > 50);
        };

        window.addEventListener('scroll', handleScroll);

        return () => {
            window.removeEventListener('scroll', handleScroll);
        };
    }, []);

    useEffect(() => {
        const handleClickOutside = (event) => {
            if (menuRef.current && !menuRef.current.contains(event.target)) {
                setIsMenuOpen(false);
            }
        };

        document.addEventListener('mousedown', handleClickOutside);

        return () => {
            document.removeEventListener('mousedown', handleClickOutside);
        };
    }, []);

    const handleMenuToggle = () => {
        setIsMenuOpen((prevState) => !prevState);
    };    

    const handleMenuClick = (menu) => {
        setActiveMenu(menu);
        setIsMenuOpen(false); // menutup menu setelah di klik
    };

    return (
        <header className={`fixed top-0 left-0 w-full p-2 px-4 md:px-20 bg-custom-orange flex justify-between items-center z-50 transition-colors duration-300 ${isScrolled ? 'bg-custom-orange' : ''}`}>
            <a href="/" className="text-xl font-bold text-white">
                <img src='suitmediaLogo.png' alt='logosuitmedia' className='h-12' />
            </a>

            <nav ref={menuRef} className={`fixed inset-y-0 right-0 w-1/2 flex flex-col items-center justify-center bg-black bg-opacity-90 transition-transform duration-300 transform ${isMenuOpen ? 'translate-x-0' : 'translate-x-full'} md:static md:bg-transparent md:bg-opacity-0 md:transform-none md:flex md:flex-row md:space-x-10 space-y-4 md:space-y-0`}>
                <a
                    href="#"
                    className={`text-white relative group ${activeMenu === 'work' ? 'underline-active' : ''}`}
                    onClick={() => handleMenuClick('work')}
                >
                    Work
                    <span className={`absolute left-0 bottom-[-2px] w-full h-0.5 bg-white transform scale-x-0 origin-left ${activeMenu === 'work' ? 'scale-x-100 transition-transform duration-300' : ''}`}></span>
                </a>
                <a
                    href="#"
                    className={`text-white relative group ${activeMenu === 'about' ? 'underline-active' : ''}`}
                    onClick={() => handleMenuClick('about')}
                >
                    About
                    <span className={`absolute left-0 bottom-[-2px] w-full h-0.5 bg-white transform scale-x-0 origin-left ${activeMenu === 'about' ? 'scale-x-100 transition-transform duration-300' : ''}`}></span>
                </a>
                <a
                    href="#"
                    className={`text-white relative group ${activeMenu === 'services' ? 'underline-active' : ''}`}
                    onClick={() => handleMenuClick('services')}
                >
                    Services
                    <span className={`absolute left-0 bottom-[-2px] w-full h-0.5 bg-white transform scale-x-0 origin-left ${activeMenu === 'services' ? 'scale-x-100 transition-transform duration-300' : ''}`}></span>
                </a>
                <a
                    href="#"
                    className={`text-white relative group ${activeMenu === 'ideas' ? 'underline-active' : ''}`}
                    onClick={() => handleMenuClick('ideas')}
                >
                    Ideas
                    <span className={`absolute left-0 bottom-[-2px] w-full h-0.5 bg-white transform scale-x-0 origin-left ${activeMenu === 'ideas' ? 'scale-x-100 transition-transform duration-300' : ''}`}></span>
                </a>
                <a
                    href="#"
                    className={`text-white relative group ${activeMenu === 'careers' ? 'underline-active' : ''}`}
                    onClick={() => handleMenuClick('careers')}
                >
                    Careers
                    <span className={`absolute left-0 bottom-[-2px] w-full h-0.5 bg-white transform scale-x-0 origin-left ${activeMenu === 'careers' ? 'scale-x-100 transition-transform duration-300' : ''}`}></span>
                </a>
                <a
                    href="#"
                    className={`text-white relative group ${activeMenu === 'contact' ? 'underline-active' : ''}`}
                    onClick={() => handleMenuClick('contact')}
                >
                    Contact
                    <span className={`absolute left-0 bottom-[-2px] w-full h-0.5 bg-white transform scale-x-0 origin-left ${activeMenu === 'contact' ? 'scale-x-100 transition-transform duration-300' : ''}`}></span>
                </a>
            </nav>

            <div className={`md:hidden flex flex-col justify-around w-8 h-5 cursor-pointer mr-10`} onClick={handleMenuToggle}>
                <div className={`w-full h-0.5 bg-white transition-transform duration-300 ${isMenuOpen ? 'transform translate-y-2 rotate-45' : ''}`}></div>
                <div className={`w-full h-0.5 bg-white transition-opacity duration-300 ${isMenuOpen ? 'opacity-0' : ''}`}></div>
                <div className={`w-full h-0.5 bg-white transition-transform duration-300 ${isMenuOpen ? 'transform -translate-y-2 -rotate-45' : ''}`}></div>
            </div>
        </header>
    );
};

export default Header;
